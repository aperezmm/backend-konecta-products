<?php 
    class Sale{
        // Connection
        private $conn;

        // Table
        private $db_table = "sale_konecta";

        // Columns
        public $id;
        public $id_product;
        public $quantity_sold;

        // BD Connection
        public function __construct($db){
            $this->conn = $db;
        }

        // Register new sale
        public function registerNewSale($id_product, $quantity_sold){
            $sqlQuery = "INSERT INTO 
                        ". $this->db_table ." 
                    SET 
                        id_product = :id_product,
                        quantity_sold= :quantity_sold";

            $stmt = $this->conn->prepare($sqlQuery);

            $id_product = htmlspecialchars(strip_tags($id_product));
            $quantity_sold = htmlspecialchars(strip_tags($quantity_sold));

            $stmt->bindParam(":id_product", $id_product);
            $stmt->bindParam(":quantity_sold", $quantity_sold);

            if($stmt->execute()){
                return true;
            }
            return false;
        }

        

    }
?>