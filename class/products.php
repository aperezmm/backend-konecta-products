<?php 
    class Product{
        // Connection
        private $conn;

        // Table
        private $db_table = "products_konecta";

        // Columns
        public $id;
        public $name;
        public $ref;
        public $price;
        public $weight;
        public $category;
        public $stock;
        public $created_at;

        // BD Connection
        public function __construct($db){
            $this->conn = $db;
        }

        // Get all products
        public function getAllProducts(){
            $sqlQuery = "SELECT id, name, ref, price, weight, category, stock, created_at FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // Create products
        public function createProduct(){
            $sqlQuery = "INSERT INTO 
                        ". $this->db_table ." 
                    SET 
                        name = :name, 
                        ref = :ref, 
                        price = :price, 
                        weight = :weight, 
                        category = :category, 
                        stock = :stock";

            $stmt = $this->conn->prepare($sqlQuery);

            $this->name = htmlspecialchars(strip_tags($this->name));
            $this->ref = htmlspecialchars(strip_tags($this->ref));
            $this->price = htmlspecialchars(strip_tags($this->price));
            $this->weight = htmlspecialchars(strip_tags($this->weight));
            $this->category = htmlspecialchars(strip_tags($this->category));
            $this->stock = htmlspecialchars(strip_tags($this->stock));

            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":ref", $this->ref);
            $stmt->bindParam(":price", $this->price);
            $stmt->bindParam(":weight", $this->weight);
            $stmt->bindParam(":category", $this->category);
            $stmt->bindParam(":stock", $this->stock);

            if($stmt->execute()){
                return true;
            }
            return false;
        }

        // Update a specific product
        public function updateProduct(){
            $sqlQuery = "UPDATE 
                        ". $this->db_table ."
                    SET 
                        name = :name, 
                        ref = :ref, 
                        price = :price, 
                        weight= :weight, 
                        category= :category, 
                        stock= :stock 
                    WHERE 
                        id = :id";

            $stmt = $this->conn->prepare($sqlQuery);

            $this->id = htmlspecialchars(strip_tags($this->id));
            $this->name = htmlspecialchars(strip_tags($this->name));
            $this->ref = htmlspecialchars(strip_tags($this->ref));
            $this->price = htmlspecialchars(strip_tags($this->price));
            $this->weight = htmlspecialchars(strip_tags($this->weight));
            $this->category = htmlspecialchars(strip_tags($this->category));
            $this->stock = htmlspecialchars(strip_tags($this->stock));

            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":ref", $this->ref);
            $stmt->bindParam(":price", $this->price);
            $stmt->bindParam(":weight", $this->weight);
            $stmt->bindParam(":category", $this->category);
            $stmt->bindParam(":stock", $this->stock);

            if($stmt->execute()){
                return true;
            }
            return false;
        }

        // Update STOCK when the ADMIN SOLD A PRODUCT
        public function updateStockProduct($id, $stock){

            $sqlQuery = "UPDATE 
                        ". $this->db_table ."
                    SET  
                        stock= IF(stock - :stock > 0, stock - :stock, stock)
                    WHERE 
                        id = :id";
            
            $stmt = $this->conn->prepare($sqlQuery);

            $id = htmlspecialchars(strip_tags($id));
            $stock = htmlspecialchars(strip_tags($stock));

            $stmt->bindParam(":id", $id);
            $stmt->bindParam(":stock", $stock);

            if($stmt->execute() && $stmt->rowCount()){
                return true;
            }
            return false;
        }

        //Delete products
        public function deleteProduct(){
            $sqlQuery = "DELETE FROM 
                        " . $this->db_table . " 
                    WHERE 
                        id = ?";
            $stmt = $this->conn->prepare($sqlQuery);

            $this->id = htmlspecialchars(strip_tags($this->id));

            $stmt->bindParam(1, $this->id);

            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>