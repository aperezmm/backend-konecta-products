<?php

    class Database {
        private $host  = 'localhost';
        private $database_name = 'konecta_php_mysql';
        private $username = 'root';
        private $password = '';

        public $conn;

        public function getConnection(){
            $this->conn = null;
            try{
                // echo "Se ha conectado exitosamente a la base de datos " . $this->database_name;
                $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
                $this->conn->exec("Set names utf8");
            }catch(PDOException $exception){
                echo "No se ha podido conectar a la base de datos: " . $exception->getMessage();
            }
            return $this->conn;
        }
    }
?>