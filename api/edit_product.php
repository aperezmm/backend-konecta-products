<?php 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

    include_once '../config/db.php';
    include_once '../class/products.php';

    $database = new Database();
    $db = $database->getConnection();
    $product = new Product($db);
    $data = json_decode(file_get_contents("php://input"));

    $product->id = $data->id;

    // fields available to update
    $product->name = $data->name;
    $product->ref = $data->ref;
    $product->price = $data->price;
    $product->weight = $data->weight;
    $product->category = $data->category;
    $product->stock = $data->stock;

    if($product->updateProduct()){
        // TODO: Verify if the product exists before updateProduct.
        http_response_code(200);
        echo json_encode( array(
            "status" => 200, 
            "message" => "Se ha actualizado exitosamente el producto"));
    }else{
        http_response_code(404);
        echo json_encode( array(
            "status" => 404,
            "message" => "No se ha logrado realizar la actualización, verifique los campos ingresados"
        ));
    }
?>