<?php 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

    include_once '../config/db.php';
    include_once '../class/products.php';

    $database = new Database();
    $db = $database->getConnection();
    $products = new Product($db);
    $stmt = $products->getAllProducts();
    $productCount = $stmt->rowCount();

    // echo json_encode($productCount);

    if($productCount > 0){
        $productArr = array();
        $productArr["products"] = array();
        $productArr["productCount"] = $productCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "name" => $name,
                "ref" => $ref,
                "price" => $price,
                "weight" => $weight,
                "category" => $category,
                "stock" => $stock,
                "created_at" => $created_at
            );

            array_push($productArr["products"], $e);
        }

        echo json_encode($productArr);
    }else{
        http_response_code(404);
        echo json_encode( array("message" => "No se han encontrado productos."));
    }

?>