<?php 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type ');

    include_once '../config/db.php';
    include_once '../class/products.php';
    include_once '../class/sales.php';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        header("HTTP/1.1 200 OK");

        $database = new Database();
        $db = $database->getConnection();
        $product = new Product($db);
        $sale = new Sale($db);

        $data = json_decode(file_get_contents("php://input"));

        $product->id = $data->id;
        $product->stock = $data->stock;

        $sale->id_product = $data->id;
        $sale->quantity_sold = $data->stock;
        
        if($product){
            if($product->updateStockProduct($product->id, $product->stock)){
                $sale->registerNewSale($sale->id_product, $sale->quantity_sold);
                http_response_code(200);
                echo json_encode( array(
                    "status" => 200, 
                    "message" => "La venta se ha realizado exitosamente"));
            }else{
                http_response_code(400);
                echo json_encode( array(
                    "status" => 400, 
                    "message" => "No hay tanto STOCK disponible"));
            }        
        }else{        
            http_response_code(404);
            echo json_encode( array(
                "status" => 404,
                "message" => "No se ha logrado realizar la actualización del stock, verifique los campos ingresados"
            ));
        }
    }
?>