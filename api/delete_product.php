<?php 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

    include_once '../config/db.php';
    include_once '../class/products.php';

    $database = new Database();
    $db = $database->getConnection();
    $product = new Product($db);
    $data = json_decode(file_get_contents("php://input"));

    $product->id = $data->id;

    

    if($product->deleteProduct()){
        http_response_code(200);
        echo json_encode( array(
            "status" => 200, 
            "message" => "El producto ha sido eliminado exitosamente",
        ));
    }else{
        http_response_code(404);
        echo json_encode( array(
            "status" => 404,
            "message" => "No se ha logrado eliminar el producto, verifique los campos ingresados"
        ));
    }

?>