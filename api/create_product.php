<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/db.php';
    include_once '../class/products.php';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        header("HTTP/1.1 200 OK");

        $database = new Database();
        $db = $database->getConnection();
        $product = new Product($db);
        $data = json_decode(file_get_contents("php://input"));

        // fields product
        $product->name = $data->name;
        $product->ref = $data->ref;
        $product->price = $data->price;
        $product->weight = $data->weight;
        $product->category = $data->category;
        $product->stock = $data->stock;

        if($product->createProduct()){
            http_response_code(200);
            echo json_encode( array(
                "status" => 200, 
                "message" => "Se ha registrado exitosamente el producto"));
        }else{
            http_response_code(404);
            echo json_encode( array(
                "status" => 404,
                "message" => "No se ha logrado realizar el registro, verifique los campos ingresados"
            ));
        }
    }

?>